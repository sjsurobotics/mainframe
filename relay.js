"use strict";

function Relay(io, production) {
	this.rover_to_mission_control = false;
	this.io = io;
	this.production = production;
	this.connections = {
		// Mission Relay Connections.
		captain: {
			connected: false,
			id: ''
		},
		navigator: {
			connected: false,
			id: ''
		},
		tracker: {
			connected: false,
			id: ''
		},
		archaeologist: {
			connected: false,
			id: ''
		},
		cortex: {
			connected: false,
			id: '',
		},
		oculus0: {
			connected: false,
			id: '',
		},
		oculus1: {
			connected: false,
			id: '',
		}
	};
	this.categories = [
		"captain", 
		"navigator", 
		"tracker", 
		"archaeologist",
		"cortex",
		"oculus0",
		"oculus1"
	];
	this.directives = {
		// "captain": "",
		"navigator": "MOTOR",
		"tracker": "TRACKER",
		"archaeologist": "ARM"
		//"cortex": "",
		//"oculus": "",
	};
}
Relay.prototype.sendTo = function(category, subject, data) {
	//console.log("category = "+category);
	if(category == "mission_control") {
		if(this.connections['captain'].id != '') {
			this.io.to(this.connections['captain'].id).emit(subject, data);
		}
		if(this.connections['navigator'].id != '') {
			this.io.to(this.connections['navigator'].id).emit(subject, data);
		}
		if(this.connections['tracker'].id != '') {
			this.io.to(this.connections['tracker'].id).emit(subject, data);
		}
		if(this.connections['archaeologist'].id != '') {
			this.io.to(this.connections['archaeologist'].id).emit(subject, data);
		}
	} else if(category == "rover" && this.rover_to_mission_control) {
		if(this.connections['cortex'].id != '') {
			this.io.to(this.connections['cortex'].id).emit(subject, data);
		}
		if(this.connections['oculus0'].id != '') {
			this.io.to(this.connections['oculus1'].id).emit(subject, data);
		}
		if(this.connections['oculus1'].id != '') {
			this.io.to(this.connections['oculus2'].id).emit(subject, data);
		}
	} else {
		// This will look up the directives per each categories
		var directive = this.directives[category];
		// If it was not found, then it does not enforce a directive.
		if(!_.isUndefined(directive)) {
			// Ex: sets to MOTOR if user is navigator.
			data['directives'] = directive;
		}
		var id = this.connections[category]["id"];
		if(id != '') {
			console.log("non-empty id = "+id);
			this.io.to(id).emit(subject, data);	
		} else {
			console.log("empty id = "+id);
			this.io.sockets.emit('SERVERSIG', {
				directive: 'NOTCONNECTED',
				info: category+" is not connected!"
			});
		}
	}
}
Relay.prototype.register = function(category, id, socket) {
	// Check to see if category even exists
	if(this.categories.indexOf(category) != -1) {
		// Check to see if entity is already logged in
		if(this.connections[category].connected == true) {
			// Send disconnect and disconnect socket
			this.io.to(id).emit('SERVERSIG', {
				directive: 'DISCONNECT',
				info: category+" is already connected!"
			});
			socket.disconnect();
			return false;
		}
		// Registration Procedure
		this.connections[category].connected = true;
		this.connections[category].id = id;
		this.io.sockets.emit('SERVERSIG', {
			directive: 'CONNECT',
			info: category
		});
		this.sendConnections(id);
		return true;
	}
	return false;
}
Relay.prototype.log = function() {}
Relay.prototype.getCategory = function(id) {
	for (var con in this.connections) {
		if(this.connections[con].id == id) {
			return con;
		}
	}
	return false;
}
Relay.prototype.getID = function(category) {
	if(this.categories.indexOf(category) != -1) {
		var id = this.connections[category]['id'];
		if(id == '') {
			return false;
		}
		return id;
	}
}
Relay.prototype.sendConnections = function(id) {
	for (var con in this.connections) {
		if(this.connections[con].id != id && this.connections[con].id != '') {
			this.io.to(id).emit('SERVERSIG', {
				directive: 'CONNECT',
				info: con
			});
		}
	}
}
Relay.prototype.checkConnectionEvents = function() {
	if(this.production == true) {
		if(this.rover_to_mission_control == false) {
			if(this.connections["captain"].connected == true &&
				this.connections["navigator"].connected == true &&
				this.connections["tracker"].connected == true &&
				this.connections["archaeologist"].connected == true &&
				this.connections["oculus"].connected == true &&
				this.connections["cortex"].connected == true) {
				console.log("BOTH Captian and Rover are connected, bidirectional stream can now start.");
				this.sendTo('rover', 'SERVERSIG', 'MISSION_CONTROL_CONNECTED');
				this.sendTo('mission_control', 'SERVERSIG', 'ROVER_CONNECTED');
				this.rover_to_mission_control = true;
			}
		} else {
			if(this.connections["captain"].connected == true &&
				this.connections["navigator"].connected == true &&
				this.connections["tracker"].connected == true &&
				this.connections["archaeologist"].connected == true &&
				this.connections["oculus"].connected == true &&
				this.connections["cortex"].connected == true) {
				console.log("BOTH Captian and Rover are connected, bidirectional stream can now start.");
				this.sendTo('rover', 'SERVERSIG', 'MISSION_CONTROL_CONNECTED');
				this.sendTo('mission_control', 'SERVERSIG', 'ROVER_CONNECTED');
				this.rover_to_mission_control = true;
			}
		}
	}
}
Relay.prototype.handleDisconnect = function(id) {
	var category = this.getCategory(id);
	if(category == false) {
		console.log("Disconnect from id "+id);
		return;
	}
	this.connections[category].connected = false;
	this.connections[category].id = '';
	this.io.sockets.emit('SERVERSIG', {
		directive: 'DISCONNECT',
		info: category
	});
	this.checkConnectionEvents();
}

module.exports = exports = Relay;