#! /bin/bash
#
# Diffusion youtube avec ffmpeg
 
# Configurer youtube avec une résolution 720p. La vidéo n'est pas scalée.
 
VBR="2500k"                                    # Bitrate de la vidéo en sortie
FPS="30"                                       # FPS de la vidéo en sortie
QUAL="medium"                                  # Preset de qualité FFMPEG
YOUTUBE_URL="rtmp://a.rtmp.youtube.com/live2"  # URL de base RTMP youtube
 
# SOURCE="udp://239.255.139.0:1234"              # Source UDP (voir les annonces SAP)
KEY="kammcecorp.140b-748y-pzdv-973c"                                     # Clé à récupérer sur l'event youtube
 
# ffmpeg \
#     -i "$SOURCE" -deinterlace \
#     -vcodec libx264 -pix_fmt yuv420p -preset $QUAL -r $FPS -g $(($FPS * 2)) -b:v $VBR \
#     -acodec libmp3lame -ar 44100 -threads 6 -qscale 3 -b:a 712000 -bufsize 512k \
#     -f flv "$YOUTUBE_URL/$KEY"

echo "$YOUTUBE_URL/$KEY"

ffmpeg -threads 4 -f alsa -i pulse \
-f v4l2 -framerate $FPS -i /dev/video0 \
-vcodec libx264 -b:v $VBR -vf "format=yuv420p" -g 60 -acodec libmp3lame -b:a 400k -ar 44100 \
-f flv "$YOUTUBE_URL/$KEY"