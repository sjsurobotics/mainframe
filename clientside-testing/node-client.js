/*
	 // send to current request socket client
	 socket.emit('message', "this is a test");

	 // sending to all clients, include sender
	 io.sockets.emit('message', "this is a test");

	 // sending to all clients except sender
	 socket.broadcast.emit('message', "this is a test");

	 // sending to all clients in 'game' room(channel) except sender
	 socket.broadcast.to('game').emit('message', 'nice game');

	  // sending to all clients in 'game' room(channel), include sender
	 io.sockets.in('game').emit('message', 'cool game');

	 // sending to individual socketid
	 io.sockets.socket(socketid).emit('message', 'for your eyes only');
 */
"use strict";

var forever = require('forever-monitor');
GLOBAL._ = require("underscore");

console.log("Starting Signal Relay");

'use strict';

var Primus = require('primus')
var Socket = Primus.createSocket()
 , client = new Socket('http://127.0.0.1:8500', { strategy: 'timeout', timeout: 5000 });

var i;
client.on('open', function open() {
  	console.log("connection!");
	i = setInterval(function() {
		console.log("interval", client.latency);
		client.write({ foo: 'bar' });
	}, 2000);
	// setTimeout(function() {
	// 	console.log("calling disconnect!")
	// 	client.end();
	// }, 60000);
	client.on('data', function message(data) {
	  console.log(data);
	});
});
client.on('end', function () {
	console.log("disconnection!");
	clearInterval(i);
});