/*
	 // send to current request socket client
	 socket.emit('message', "this is a test");

	 // sending to all clients, include sender
	 io.sockets.emit('message', "this is a test");

	 // sending to all clients except sender
	 socket.broadcast.emit('message', "this is a test");

	 // sending to all clients in 'game' room(channel) except sender
	 socket.broadcast.to('game').emit('message', 'nice game');

	  // sending to all clients in 'game' room(channel), include sender
	 io.sockets.in('game').emit('message', 'cool game');

	 // sending to individual socketid
	 io.sockets.socket(socketid).emit('message', 'for your eyes only');
 */
"use strict";

var forever = require('forever-monitor');
GLOBAL._ = require("underscore");

console.log("Starting Signal Relay");

var io = require('socket.io').listen(8085, {
	pingTimeout: 3000,
	pingInterval: 2000,
});
var express = require('express');
var Relay = require('./relay.js');
var fs = require('fs');
//var MjpegProxy = require('mjpeg-proxy').MjpegProxy;

var production = false;
var relay = new Relay(io, production);

// var pass = io.of('/passengers');
// pass.on('connection', function(socket) {
//   console.log('Passenger Connected');
// });

var map = io.of('/map');
var map_struct = {};
map.on('connection', function(socket) {
	console.log('MAP Connected');
	socket.on('MAP', function (data) {
		console.log("MAP EVENT!", data);
		if(data["directive"] == "SET") {
			map_struct = data["info"];
			console.log("Map strcut = ", map_struct);
		}
		socket.broadcast.emit('MAP', map_struct);
	});
});

var sensors = {test: 0};
//// Setting up communication for all everyone
io.sockets.on('connection', function (socket) {
	console.log("An Entity has been connected." + socket.id);
	socket.on('REGISTER', function (data) { 
		var entity = data["entity"];
		// if(entity == "debug" && production == false) {
		// 	relay.register(entity, socket.id);
		// }
		if(data["password"] != "destroymit") {
			console.log("Password incorrect for: "+entity);
			io.to(socket.id).emit("SERVERSIG","PASSWORD_INCORRECT");
			socket.disconnect();
		} 
		if(relay.register(entity, socket.id, socket)) {
			console.log("Registation Complete for entity: "+entity);
			relay.log(data);
		}
	});
	socket.on('CTRLSIG', function (data) { 
		if(production == true && relay.rover_to_mission_control == false) { return; }
		var oculuar_signals = ["VIDEO", "AUDIO", "OCULUS1", "OCULUS2"];
		if(oculuar_signals.indexOf(data["directive"]) != -1) {
			console.log("Transmitting OCULARSIG", data);
			relay.sendTo('oculus0', 'CTRLSIG', data);
			relay.sendTo('oculus1', 'CTRLSIG', data);	
		} else {
			console.log("Transmitting CTRLSIG", data);
			relay.sendTo('cortex', 'CTRLSIG', data);
		}
	});
	socket.on('OCULARSIG', function (data) { 
		if(production == true && relay.rover_to_mission_control == false) { return; }
		if(data["directive"] != "SENSORS") {
			console.log("Transmitting OCULARSIG", data);
			relay.sendTo("mission_control", 'OCULARSIG', data);
		} else {
			relay.sendTo("captain", 'OCULARSIG', data);
		}
	});
	socket.on('ROVERSIG', function (data) {
		if(production == true && relay.rover_to_mission_control == false) { return; }
		if(data["directive"] != "SENSORS") {
			console.log("Transmitting ROVERSIG", data);	
		}
		relay.sendTo("mission_control", 'ROVERSIG', data);
	});
	socket.on('SERVERSIG', function (data) {});
	socket.on('disconnect', function () {
		console.log(((relay.getCategory(socket.id) == false) ? "undefined entity" : relay.getCategory(socket.id)) + " disconnected");
		relay.handleDisconnect(socket.id);
		//io.socket.connected[socket.id].disconnect();
	});
});

var vstream1 = forever.start([ 'cvlc', '--live-caching', '0', 'udp://@:9000', '":sout=#standard{access=http{mime=multipart/x-mixed-replace;boundary=--7b3cc56e5f51db803f790dad720ed50a},mux=mpjpeg,dst=:9001}}"', '-I', 'dummy' ], {
    max : 20,
    silent : false
});
var vstream2 = forever.start([ 'cvlc', '--live-caching', '0', 'udp://@:9002', '":sout=#standard{access=http{mime=multipart/x-mixed-replace;boundary=--7b3cc56e5f51db803f790dad720ed50a},mux=mpjpeg,dst=:9003}}"', '-I', 'dummy' ], {
    max : 20,
    silent : false
});
var vstream3 = forever.start([ 'cvlc', '--live-caching', '0', 'udp://@:9003', '":sout=#standard{access=http{mime=multipart/x-mixed-replace;boundary=--7b3cc56e5f51db803f790dad720ed50a},mux=mpjpeg,dst=:9004}}"', '-I', 'dummy' ], {
    max : 20,
    silent : false
});
var astream1 = forever.start([ 'cvlc', 'rtp://:9005', '--sout', '#standard{access=http,mux=mp3,dst=:9006/stream}', '-I', 'dummy' ], {
    max : 20,
    silent : false
});
//vstream2.start();
var astream2 = forever.start([ 'cvlc', 'rtp://:9007', '--sout', '#standard{access=http,mux=mp3,dst=:9008/stream}', '-I', 'dummy' ], {
    max : 20,
    silent : false
});