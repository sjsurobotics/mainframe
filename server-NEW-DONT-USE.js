/*
	 // send to current request socket client
	 socket.emit('message', "this is a test");

	 // sending to all clients, include sender
	 io.sockets.emit('message', "this is a test");

	 // sending to all clients except sender
	 socket.broadcast.emit('message', "this is a test");

	 // sending to all clients in 'game' room(channel) except sender
	 socket.broadcast.to('game').emit('message', 'nice game');

	  // sending to all clients in 'game' room(channel), include sender
	 io.sockets.in('game').emit('message', 'cool game');

	 // sending to individual socketid
	 io.sockets.socket(socketid).emit('message', 'for your eyes only');
 */
"use strict";

var forever = require('forever-monitor');
GLOBAL._ = require("underscore");

console.log("Starting Signal Relay");

'use strict';

var Primus = require('primus')
  , http = require('http');

var server = http.createServer(/* request handler */).listen(8500)
  , primus = new Primus(server, { timeout: 2500 });

primus.on('connection', function (spark) {
	console.log("connection", spark);
	spark.on('data', function message(data) {
	  console.log(data);
	  spark.write(data);
	});
});
primus.on('disconnection', function (spark) {
	console.log("disconnection!", spark);
});



var vstream1 = forever.start([ 'cvlc', '--live-caching', '0', 'udp://@:9000', '":sout=#standard{access=http{mime=multipart/x-mixed-replace;boundary=--7b3cc56e5f51db803f790dad720ed50a},mux=mpjpeg,dst=:9001}}"', '-I', 'dummy' ], {
    max : 20,
    silent : false
});
var vstream2 = forever.start([ 'cvlc', '--live-caching', '0', 'udp://@:9002', '":sout=#standard{access=http{mime=multipart/x-mixed-replace;boundary=--7b3cc56e5f51db803f790dad720ed50a},mux=mpjpeg,dst=:9003}}"', '-I', 'dummy' ], {
    max : 20,
    silent : false
});
var vstream3 = forever.start([ 'cvlc', '--live-caching', '0', 'udp://@:9003', '":sout=#standard{access=http{mime=multipart/x-mixed-replace;boundary=--7b3cc56e5f51db803f790dad720ed50a},mux=mpjpeg,dst=:9004}}"', '-I', 'dummy' ], {
    max : 20,
    silent : false
});
var astream1 = forever.start([ 'cvlc', 'rtp://:9005', '--sout', '#standard{access=http,mux=mp3,dst=:9006/stream}', '-I', 'dummy' ], {
    max : 20,
    silent : false
});
//vstream2.start();
var astream2 = forever.start([ 'cvlc', 'rtp://:9007', '--sout', '#standard{access=http,mux=mp3,dst=:9008/stream}', '-I', 'dummy' ], {
    max : 20,
    silent : false
});